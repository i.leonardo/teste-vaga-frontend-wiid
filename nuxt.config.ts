import { NuxtConfig } from '@nuxt/types'

const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'spa',
  /*
   ** Define the source directory of your Nuxt.js application
   ** See https://nuxtjs.org/api/configuration-srcdir/
   */
  srcDir: 'src/',
  /*
   ** Nuxt environment variables
   ** See https://nuxtjs.org/api/configuration-env
   */
  env: {
    version: process.env.npm_package_version,
  },
  /*
   ** Skip Prompt
   ** See https://github.com/nuxt/telemetry#opting-out
   */
  telemetry: false,
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    titleTemplate: '%s | WiiD',
    title: 'Work In Ideas',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Nunito&display=swap',
      },
    ],
  },
  router: {
    base: process.env.NODE_ENV === 'production' ? base : '/',
  },
  /*
   ** Global CSS
   */
  css: ['@/assets/scss/global.scss'],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: ['@/plugins/auth.ts'],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    '@nuxt/typescript-build',
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    // Doc: https://github.com/nuxt-community/vuetify-module
    '@nuxtjs/vuetify',
    // Doc: https://github.com/microcipcip/cookie-universal/tree/master/packages/cookie-universal-nuxt
    'cookie-universal-nuxt',
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/pwa',
    // Doc: https://axios.nuxtjs.org/
    '@nuxtjs/axios',
  ],

  vuetify: {
    treeShake: true,
    customVariables: ['@/assets/scss/variables.scss'],
    defaultAssets: {
      font: false,
      icons: 'mdi',
    },
    optionsPath: '@@/vuetify.options.js',
  },

  axios: {
    baseURL: 'http://my-json-server.typicode.com/workinideas/vagafrontendteste',
  },

  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    postcss: {
      plugins: {
        // https://browserl.ist
        autoprefixer: {},
      },
    },

    extend(_config, { isClient, loaders: { vue } }) {
      if (isClient && vue) {
        vue.transformAssetUrls = {
          VImg: ['src', 'lazy-src'],
          'v-img': ['src', 'lazy-src'],
        }
      }
    },
  },
} as NuxtConfig
