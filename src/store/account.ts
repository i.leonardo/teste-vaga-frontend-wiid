import { GetterTree, MutationTree, ActionTree } from 'vuex'
import jwt from 'jsonwebtoken'

import { objAssign } from '@/utils/vuex'

export const state = () => ({
  user: {
    uid: '',
    itemEmail: null,
  },

  profile: {
    name: '',
    photoUrl: '',
  },
})

export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  setUser: objAssign('user'),
  setProfile: objAssign('profile'),
}

export const getters: GetterTree<RootState, RootState> = {
  isAuthenticated(store) {
    return !!store.user && !!store.user.uid
  },
}

export const actions: ActionTree<RootState, RootState> = {
  signIn({ dispatch }, payload) {
    return new Promise((resolve, reject) => {
      if (payload.name === 'admin' && payload.password === 'admin') {
        dispatch('tokenRefresh')
        resolve()
      } else {
        reject(new Error('Usuário não encontrado'))
      }
    })
  },

  signOut({ commit }) {
    return new Promise((resolve, reject) => {
      if (this.$cookies.get('access_token')) {
        this.$cookies.remove('access_token')
        commit('setUser', { uid: '' })
        resolve()
      } else {
        reject(new Error('Usuário não conectado'))
      }
    })
  },

  checkUserExist({ commit }) {
    return new Promise((resolve, reject) => {
      const token = this.$cookies.get('access_token')

      try {
        const decoded = jwt.verify(token, 'shhhhh')
        commit('setUser', decoded)
        commit('setProfile', { name: 'Admin' })
        resolve('Conectado!')
      } catch (err) {
        reject(err)
      }
    })
  },

  tokenRefresh() {
    const exp = new Date().setHours(new Date().getHours() + 1)
    const token = jwt.sign(
      {
        uid: '0000-0000-0000',
        exp,
      },
      'shhhhh'
    )
    this.$cookies.set('access_token', token)

    if (process.env.NODE_ENV === 'development') {
      console.info({
        token,
        expirationTime: new Date(exp).toLocaleString(),
      })
    }
  },

  initAccount({ commit }) {
    commit('setUser', { uid: '', itemEmail: null })
    commit('setProfile', { name: '', photoUrl: '' })
  },
}
