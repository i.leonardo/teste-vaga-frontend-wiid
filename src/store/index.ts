import { MutationTree, ActionTree } from 'vuex'
import { set, toggle } from '@/utils/vuex'

export const strict = false

export const state = () => ({
  version: process.env.version,
  debug: process.env.NODE_ENV === 'development',

  popup: {
    status: false,
    msg: '',
    type: '',
    config: {},
  },
  dark: false,
})
export type RootState = ReturnType<typeof state>

export const mutations: MutationTree<RootState> = {
  setPopup: set('popup'),
  setBoolPopup: set('popup.status'),
  setDark: set('dark'),
  toggleDark: toggle('dark'),
}

export const actions: ActionTree<RootState, RootState> = {}
