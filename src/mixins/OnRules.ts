import { Vue, Component } from 'nuxt-property-decorator'

import { isNumber } from '@/utils'

@Component
export default class OnRules extends Vue {
  get rules() {
    return {
      required: (v: string) => !!v || 'Campo obrigatório',
      number: (v: string) => (v && isNumber(v)) || 'Apenas Números',
      array: (v: []) => (v && v.length > 0) || 'Campo obrigatório',
      object: (v: object) => Object.keys(v).length > 0 || 'Campo obrigatório',
      date: (v: string) => (v && v.length === 10) || 'Campo obrigatório',
      email: (v: string) =>
        /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(v) || 'E-mail deve ser válido',
      minLenght: (min: number, msg: string) => (v: string) =>
        (v && v.toString().length >= min) || msg,
      maxLenght: (min: number, msg: string) => (v: string) =>
        (v && v.toString().length <= min) || msg,
    }
  }
}
