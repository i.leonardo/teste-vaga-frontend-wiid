import { Component, Vue } from 'nuxt-property-decorator'

@Component
export default class ChangeLogout extends Vue {
  getFirstLetter(val: string) {
    return val.substring(0, 1)
  }
}
