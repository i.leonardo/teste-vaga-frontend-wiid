import { Vue, Component, Mutation } from 'nuxt-property-decorator'
import { RootState } from '@/store'

import { wait } from '@/utils'

@Component
export default class OnMsg extends Vue {
  @Mutation
  setPopup!: (val: RootState['popup']) => void

  @Mutation
  setBoolPopup!: (bool: boolean) => void

  get isStatus() {
    return (this.$store.state as RootState).popup.status
  }

  async onMsg(msg: string, type = 'warning', opts = {}) {
    const config = {
      timeout: 6000,
      icon: undefined,
      bind: { right: true },
    }

    if (this.isStatus) {
      await wait(250) // visualizar ultima msg
      this.setBoolPopup(false)
      await wait(250) // efeito para entrar outra msg
    }

    if (Object.keys(opts).length > 0) {
      Object.assign(config, opts)
    }

    this.setPopup({
      msg,
      type,
      status: true,
      config,
    })
  }
}
