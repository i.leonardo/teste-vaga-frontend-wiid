import { mixins, Component, namespace } from 'nuxt-property-decorator'

import { onLogout, errorServer } from '@/data/msg.json'
import OnMsg from './OnMsg'

const accountModule = namespace('account')

@Component
export default class ChangeLogout extends mixins(OnMsg) {
  @accountModule.Action('signOut')
  signOut!: () => Promise<void>

  async changeLogout(fullPath?: string) {
    try {
      await this.signOut()

      this.onMsg(onLogout, 'success')
      this.$router.resolve({ path: '/login' }).route.meta.user = null

      const next = fullPath ? `/?next=${encodeURI(fullPath)}` : ''
      this.$router.push(`/login${next}`)
    } catch (err) {
      console.error(err)

      this.onMsg(errorServer, 'error')
    }
  }
}
