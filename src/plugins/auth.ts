import { Plugin } from '@nuxt/types'

// export default (ctx: Plugin.Context) => new Promise((resolve) => {
//   const token = ctx.store.
// })

const myPlugin: Plugin = ({ store }) => {
  return new Promise((resolve) => {
    store.dispatch('account/checkUserExist')
    resolve()
  })
}

export default myPlugin
