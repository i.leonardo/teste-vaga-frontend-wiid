import { Context } from '@nuxt/types'

import { getUrlNext } from '@/utils'

export default function ({ store, route, redirect }: Context) {
  if (store.getters['account/isAuthenticated'] && route.name === 'login') {
    return redirect(getUrlNext(route.query))
  } else if (
    !store.getters['account/isAuthenticated'] &&
    route.name !== 'login'
  ) {
    return redirect(`/login/?next=${encodeURI(route.fullPath)}`)
  }
  return false
}
