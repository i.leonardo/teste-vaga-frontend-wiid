import { Route } from 'vue-router'
import objectPath from 'object-path'

// Check key exist Object
export const has = (obj: Object, property: string | string[]) => {
  return objectPath.has(obj, property)
}

// Promise setTimeout
export const wait = (ms: number) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

// isNaN
export const isNumber = (val: string) => {
  if (/^-?\d*\.?\d+$/.test(val)) {
    return !Number.isNaN(parseFloat(val))
  }
  return false
}

// Get Url next
export const getUrlNext = (query: Route['query']) => {
  if (has(query, 'next') && query.next !== null) {
    return decodeURI(String(query.next))
  } else {
    return '/'
  }
}
