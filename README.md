# test-workinideas-frontend

> Nuxt.js TypeScript project

Apenas HTTP para funcionar API devido o CORS: http://i.leonardo.gitlab.io/teste-vaga-frontend-wiid

![main1](https://gitlab.com/i.leonardo/teste-vaga-frontend-wiid/-/raw/master/main1.png)

![main2](https://gitlab.com/i.leonardo/teste-vaga-frontend-wiid/-/raw/master/main2.png)

Contém um sistema Token para simular autenticação e bloqueio de páginas usando middleware, desenvolvido em Vuetify v2 e implementado o redimensionamento do painel.

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
